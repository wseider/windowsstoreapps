﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Windows.Web.Syndication;
using GetStarted.Data;

namespace GetStarted.Common
{
    class BlogFeedProxy
    {
        public async Task<IEnumerable<SampleDataGroup>> LoadFeedsAsync()
        {
            SyndicationClient client = new SyndicationClient();
            SyndicationFeed feed = await client.RetrieveFeedAsync(new Uri("http://www.tagesschau.de/xml/rss2"));

            SampleDataGroup group = new SampleDataGroup("OnlyOneGroup", "Nachrichten", "Nachrichten", "Assets/DarkGray.png", feed.Subtitle.Text);

            foreach (var item in feed.Items)
            {
                var images = item.Summary.Text.GetImageSources();

                SampleDataItem sampleDateItem = new SampleDataItem(
                    item.Id,
                    item.Title.Text,
                    string.Empty,
                    images.FirstOrDefault(),
                    item.Summary.Text,
                    item.Summary.Text,
                    group
                );
                group.Items.Add(sampleDateItem);
            }

            List<SampleDataGroup> groups = new List<SampleDataGroup>();
            groups.Add(group);

            return groups;
        }
    }
}

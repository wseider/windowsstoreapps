﻿using GetStarted.Data;
using System.Threading.Tasks;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using GetStarted.Common;

// Die Elementvorlage für die Seite "Gruppierte Elemente" ist unter http://go.microsoft.com/fwlink/?LinkId=234231 dokumentiert.

namespace GetStarted
{
    /// <summary>
    /// Eine Seite, auf der eine gruppierte Auflistung von Elementen angezeigt wird.
    /// </summary>
    public sealed partial class GroupedItemsPage : GetStarted.Common.LayoutAwarePage
    {
        public GroupedItemsPage()
        {
            this.InitializeComponent();

            Loaded += GroupItemsPage_Loaded;
        }

        async void GroupItemsPage_Loaded(object sender, RoutedEventArgs e)
        {
            VisualStateManager.GoToState(this, "LoadData", true);

            await Task.Delay(TimeSpan.FromSeconds(2));

            BlogFeedProxy proxy = new BlogFeedProxy();
            var feeds = await proxy.LoadFeedsAsync();
            this.DefaultViewModel["Groups"] = feeds;

            VisualStateManager.GoToState(this, "LoadDataComplete", true);
        }

        /// <summary>
        /// Füllt die Seite mit Inhalt auf, der bei der Navigation übergeben wird. Gespeicherte Zustände werden ebenfalls
        /// bereitgestellt, wenn eine Seite aus einer vorherigen Sitzung neu erstellt wird.
        /// </summary>
        /// <param name="navigationParameter">Der Parameterwert, der an
        /// <see cref="Frame.Navigate(Type, Object)"/> übergeben wurde, als diese Seite ursprünglich angefordert wurde.
        /// </param>
        /// <param name="pageState">Ein Wörterbuch des Zustands, der von dieser Seite während einer früheren Sitzung
        /// beibehalten wurde. Beim ersten Aufrufen einer Seite ist dieser Wert NULL.</param>
        protected override async void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
            // der Methodenrumpf von GroupItemsPage_Loaded wurde aus "LoadState herausgenommen da die
            //  Nutzung des VisualStateManagers in Kombination mit LayoutAwarePage nicht korrekt funktioniert
            //
            // Delegate GroupItemsPage_Loaded wurde alternativ in der Methode GroupedItemsPage eingeklinkt
        }

        /// <summary>
        /// Wird aufgerufen, wenn auf einen Gruppenkopf geklickt wird.
        /// </summary>
        /// <param name="sender">Die Schaltfläche, die als Gruppenkopf für die ausgewählte Gruppe verwendet wird.</param>
        /// <param name="e">Ereignisdaten, die beschreiben, wie der Klick initiiert wurde.</param>
        void Header_Click(object sender, RoutedEventArgs e)
        {
            // Ermitteln, welche Gruppe die Schaltflächeninstanz darstellt
            var group = (sender as FrameworkElement).DataContext;

            // Zur entsprechenden Zielseite navigieren und die neue Seite konfigurieren,
            // indem die erforderlichen Informationen als Navigationsparameter übergeben werden
            this.Frame.Navigate(typeof(GroupDetailPage), ((SampleDataGroup)group));
        }

        /// <summary>
        /// Wird aufgerufen, wenn auf ein Element innerhalb einer Gruppe geklickt wird.
        /// </summary>
        /// <param name="sender">GridView (oder ListView, wenn die Anwendung angedockt ist),
        /// die das angeklickte Element anzeigt.</param>
        /// <param name="e">Ereignisdaten, die das angeklickte Element beschreiben.</param>
        void ItemView_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Zur entsprechenden Zielseite navigieren und die neue Seite konfigurieren,
            // indem die erforderlichen Informationen als Navigationsparameter übergeben werden
            var feedItem = ((SampleDataItem)e.ClickedItem);
            this.Frame.Navigate(typeof(ItemDetailPage), feedItem);
        }
    }
}

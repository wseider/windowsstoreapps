﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Windows.Web.Syndication;
using GetStarted.Data;

namespace GetStarted.Common
{
    class BlogFeedProxy
    {
        // 1. Simple Method-body without Async
        //      public IEnumerable<SampleDataGroup> LoadFeeds()

        // 2. Retrieve RSS-Feeds

        // 3. Modify for async

        // 4. Analyse Data from source in Debug-Mode

        // 5. Insert foreach to fill sampleDataItem's/group/groups

        public async Task<IEnumerable<SampleDataGroup>> LoadFeedsAsync()
        {
            SyndicationClient client = new SyndicationClient();
            SyndicationFeed feed = await client.RetrieveFeedAsync(new Uri("http://www.tagesschau.de/xml/rss2"));

            SampleDataGroup group = new SampleDataGroup("onlyOneGroup", "Nachrichten", "Nachrichten",
                "Assets/DarkGray.png", feed.Subtitle.Text);

            foreach (var item in feed.Items)
            {
                var images = item.Summary.Text.GetImageSources();

                SampleDataItem sampleDataItem = new SampleDataItem(item.Id, item.Title.Text, string.Empty,
                    images.FirstOrDefault(), item.Summary.Text, item.Summary.Text);
                group.Items.Add(sampleDataItem);
            }

            List<SampleDataGroup> groups = new List<SampleDataGroup>();
            groups.Add(group);

            return groups;
        } 
    }
}

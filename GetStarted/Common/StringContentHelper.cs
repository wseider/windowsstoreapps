﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace GetStarted.Common
{
    public static class StringContentHelper
    {
        public static List<string> GetImageSources(this string htmlContent)
        {
            Regex regex = new Regex("<img\\s[^>]*?src=[\"']([^\"']+)[\"'][^>]*>", RegexOptions.IgnoreCase);
            MatchCollection matches = regex.Matches(htmlContent);

            //List<string> sources = new List<string>();

            //foreach (Match item in match)
            //{
            //    sources.Add(item.Groups[1].Value);
            //}

            //return sources;

            return (from Match item in matches select item.Groups[1].Value).ToList();
        } 
    }
}
